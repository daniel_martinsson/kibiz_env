import React from "react";
import { render, screen } from "@testing-library/react";
import App from "./App";

test("that it renders", () => {
  render(<App />);

  expect(screen.getByText("App")).toBeVisible;
});
