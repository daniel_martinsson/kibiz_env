import React from "react";
import "./App.css";

function App(): JSX.Element {
  return (
    <div className="app">
      <h1>App</h1>
    </div>
  );
}

export default App;
